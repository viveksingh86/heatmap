﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            int opnum, size;
            size = 0;
            opnum = 0;
            string grid;
            Console.WriteLine("Please enter number of output, Grid size and Grid Content to genrate the Heat Map");
            grid = Console.ReadLine();
           
            var gridspilt  = grid.Split(' ');
            List<int> gridvalue = new List<int>();
            for (int i =0;i < gridspilt.Count();i++)
            {
                opnum = Convert.ToInt32(gridspilt[0]);
                size = Convert.ToInt32(gridspilt[1]);
                if (i > 1)
                gridvalue.Add(Convert.ToInt32(gridspilt[i]));
            }
            var gridsize = size*size;
            int[,] Cord = new int[size+2,size+2];
            if (gridsize != gridvalue.Count())
            {
                Console.WriteLine("Grid Values Not Proper");

            }
            else
            {
                int x = 1;
                int y = 1;

                for (int i = 0; i < gridvalue.Count; ++i)
                {
                  
                    Cord[x,y] = gridvalue[i]; // note the swap
                    
                    y++;
                    if (y == size+1)
                    {
                        y = 1;
                        x++;
                    }
                    
               
                }
            }

            CalculateHeatMap(Cord, size, gridvalue.Count(),opnum);
            Console.ReadLine();
        }
        

        public static void CalculateHeatMap(int[,] grid, int size, int gridtotal,int opnum)
        {
            List<GridStore> gridvalue = new List<GridStore>();
            int[,] Points = new int[size, size];
           // int PointValue = 0, xcord = 0 , ycord = 0;
            int x = 0;
            int y = 0;

            int x1 = 0;
            int y1 = 0;

            for (int i = 0; i < gridtotal; ++i)
            {
                x = x1+1;
                y = y1+1;
                Points[x1, y1] += grid[x - 1, y - 1];
                Points[x1, y1] += grid[x - 1, y];
                Points[x1, y1] += grid[x - 1, y + 1];
                Points[x1, y1] += grid[x, y - 1];
                Points[x1, y1] += grid[x, y];
                Points[x1, y1] += grid[x, y + 1];
                Points[x1, y1] += grid[x + 1, y - 1];
                Points[x1, y1] += grid[x + 1, y];
                Points[x1, y1] += grid[x + 1, y + 1];


                gridvalue.Add(new GridStore{xcord= x1,ycord= y1,value= Points[x1, y1]});
                y1++;
                if (y1 == size)
                {
                    y1 = 0;
                    x1++;
                }

            }
           
            var cnt = 0;

            var desc = gridvalue.OrderByDescending(t=>t.value);
            //gridvalue.Sort((t1, t2) => t1.value.CompareTo(t2.value));
            foreach (var item in desc)
            {
                if (cnt < opnum)
                {
                    Console.WriteLine(String.Format("Highest Point Value ({0},{1}) score: {2}", item.xcord, item.ycord, item.value));
                    cnt++;
                }
               
            }
           
                        

        }


    }


}
